/**
 * Object respresenting a contact
 */
export class Contact {
    Name: string;
    Email: string;
    Phone: string;
}
