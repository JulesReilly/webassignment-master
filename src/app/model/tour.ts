import { Contact } from './contact';
import { Traveller } from './traveller';
import { Trip } from './trip';
/**
 * Object respresenting a tour/s
 */
export class Tour {
    Name: string;
    ArtistName: string;
    Id: number;
    DateStart: Date;
    DateEnd: Date;
    Host: string;
    Trips: Trip[];
    Travellers: Traveller[];
    Contact: Contact;
}
