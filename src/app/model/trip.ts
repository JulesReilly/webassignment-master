/**
 * Object representing a trip
 */
export class Trip {
    Travellers: number[];
    Company: string;
    Date: Date;
    TimeDepart: string;
}
