import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BaseRequestOptions,
  Http,
  HttpModule,
  Response,
  XHRBackend,
} from '@angular/http';

import {MockBackend} from '@angular/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

import { TourDetailsComponent } from './tour-details.component';
import { CommonService } from '../../services/services';


describe('TourDetailsComponent', () => {
  let component: TourDetailsComponent;
  let fixture: ComponentFixture<TourDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourDetailsComponent ],
      imports: [
        HttpModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        CommonService,
        { provide: XHRBackend, useClass: MockBackend },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
