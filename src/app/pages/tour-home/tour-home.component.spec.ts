import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BaseRequestOptions,
  Http,
  HttpModule,
  Response,
  XHRBackend,
} from '@angular/http';

import {MockBackend} from '@angular/http/testing';

import { TourHomeComponent } from './tour-home.component';
import { RoutingModule } from '../../routing/routing.module';
import { CommonService } from '../../services/services';

describe('TourHomeComponent', () => {
  let component: TourHomeComponent;
  let fixture: ComponentFixture<TourHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourHomeComponent ],
      imports: [
        HttpModule,
      ],
      providers: [
        CommonService,
        { provide: XHRBackend, useClass: MockBackend },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
