import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BaseRequestOptions,
  Http,
  HttpModule,
  Response,
  XHRBackend,
} from '@angular/http';

import {MockBackend} from '@angular/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

import { TourTravellersComponent } from './tour-travellers.component';
import { CommonService } from '../../services/services';

describe('TourTravellersComponent', () => {
  let component: TourTravellersComponent;
  let fixture: ComponentFixture<TourTravellersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourTravellersComponent ],
      imports: [
        HttpModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        CommonService,
        { provide: XHRBackend, useClass: MockBackend },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourTravellersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
