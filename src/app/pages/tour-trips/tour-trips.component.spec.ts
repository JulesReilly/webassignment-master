import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BaseRequestOptions,
  Http,
  HttpModule,
  Response,
  XHRBackend,
} from '@angular/http';

import {MockBackend} from '@angular/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

import { TourTripsComponent } from './tour-trips.component';
import { CommonService } from '../../services/services';

describe('TourTripsComponent', () => {
  let component: TourTripsComponent;
  let fixture: ComponentFixture<TourTripsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourTripsComponent ],
      imports: [
        HttpModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        CommonService,
        { provide: XHRBackend, useClass: MockBackend },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourTripsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
