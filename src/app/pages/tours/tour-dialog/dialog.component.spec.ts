import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import {
    ElementRef,
    NO_ERRORS_SCHEMA,
    ChangeDetectorRef,
  } from '@angular/core';
import {
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA,
} from '@angular/material';

import { DialogComponent } from './dialog.component';

describe('DialogComponent', () => {
    let component: DialogComponent;
    let fixture: ComponentFixture<DialogComponent>;

    const mockDialogRef = jasmine.createSpy('MatDialogRef');

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ DialogComponent ],
            providers: [
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: {} }
            ],
            schemas: [
                NO_ERRORS_SCHEMA,
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        spyOn(component, 'closeDialog').and.returnValue(mockDialogRef);
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('should close the dialog', () => {
        component.closeDialog();
        expect(component.closeDialog).toHaveBeenCalled();
    });
});
