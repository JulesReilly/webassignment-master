import {
    Component,
    Inject,
} from '@angular/core';
import {
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA,
} from '@angular/material';

import { Tour } from '../../../model/tour';


@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.css']
})
export class DialogComponent {
    public dialogRef: MatDialogRef<DialogComponent>;
    public panelOpenState: boolean;

    private tour: Tour;

  constructor(
        dialogRef: MatDialogRef<DialogComponent>,
        @Inject(MAT_DIALOG_DATA) data,
    ) {
        this.dialogRef = dialogRef;
        this.panelOpenState = false;
        this.tour = data;
    }

    /**
     * Close modal dialog
     */
    public closeDialog = (): void => {
        this.dialogRef.close();
    }
}
