import {
    async,
    ComponentFixture,
    TestBed
} from '@angular/core/testing';
import {
    ChangeDetectorRef,
    ElementRef,
    NO_ERRORS_SCHEMA,
} from '@angular/core';
import {
    BaseRequestOptions,
    Http,
    HttpModule,
    Response,
    XHRBackend,
} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {
    MatDialog,
    MatDialogRef,
    MatDialogModule,
    MatDialogContainer,
    MAT_DIALOG_DATA,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { ToursComponent } from './tours.component';
import { DialogComponent } from './tour-dialog/dialog.component';
import { Tour } from '../../model/tour';
import { RoutingModule } from '../../routing/routing.module';
import {
    CommonService,
    TourService,
} from '../../services/services';


describe('ToursComponent', () => {
    let component: ToursComponent;
    let fixture: ComponentFixture<ToursComponent>;
    let elementRef: ElementRef;

    const mockDialogRef = jasmine.createSpy('MatDialogRef');

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                DialogComponent,
                ToursComponent
            ],
            imports: [
                BrowserAnimationsModule,
                HttpModule,
            ],
            providers: [
                CommonService,
                DialogComponent,
                { provide: MatDialog, useValue: {} },
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: {} },
                TourService,
                { provide: XHRBackend, useClass: MockBackend },
            ],
            schemas: [
                NO_ERRORS_SCHEMA,
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ToursComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        elementRef = fixture.elementRef;

        spyOn(component, 'openDialog').and.returnValue(mockDialogRef);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call ngOnInit and return void', () => {
        spyOn(component, 'ngOnInit').and.callThrough();
        component.ngOnInit();
        expect(component.ngOnInit).toHaveBeenCalledTimes(1);
    });

    it('should open details dialog', () => {
        const mockTour = new Tour();
        component.openDialog(mockTour);
        expect(component.openDialog).toHaveBeenCalledTimes(1);
  });
});
