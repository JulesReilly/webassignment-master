import {
    Component,
    OnInit
} from '@angular/core';
import {
    MatDialog,
    MatDialogRef,
} from '@angular/material';

import { DialogComponent } from './tour-dialog/dialog.component';
import { Tour } from '../../model/tour';
import { Tours } from '../../model/tours';
import { Traveller } from '../../model/traveller';
import {
    CommonService,
    TourService,
} from '../../services/services';


/**
* Initial page displaying list of available tours
*/
@Component({
    selector: 'app-tours',
    templateUrl: './tours.component.html',
    styleUrls: ['./tours.component.css']
})
export class ToursComponent implements OnInit {
    public selectedTourDialogRef: MatDialogRef<DialogComponent>;
    public tours: Tours;

    private selectedTour: Tour;

    constructor(
        private dialog: MatDialog,
        private tourService: TourService,
        private commonService: CommonService,
    ) { }

    public ngOnInit(): void {
        if (!localStorage.getItem('tours')) {
            this.getTours();
        } else {
            this.tours = JSON.parse(localStorage.getItem('tours'));
        }

        if (this.tours) {
            this.selectedTour = this.tours[0];
        }
    }

    /**
     * Open modal dialog to display selected tour
     * @param {Tour} tour
     */
    public openDialog = (tour: Tour): void => {
        this.selectedTourDialogRef = this.dialog
            .open(DialogComponent,  {
              data: tour
            });
    }

    /**
     * Selected tour click
     */
    public tourClicked = (tour): void => {
        this.selectedTour = tour;
    }

    /**
     * Retrieves list of tours to display
     */
    private getTours = (): void => {
        this.commonService.getTour().subscribe((tours: Tours) => {
            if (!tours) {
                throw new Error('Failed to retrieve tour data');
            } else {
                this.tours = tours;
            }
        });
    }
}
