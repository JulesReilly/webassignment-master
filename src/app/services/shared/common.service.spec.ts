import {
    inject,
    TestBed,
} from '@angular/core/testing';
import {
    BaseRequestOptions,
    Http,
    HttpModule,
    Response,
    XHRBackend,
} from '@angular/http';

import {MockBackend} from '@angular/http/testing';

import { CommonService } from './common.service';

describe('CommonService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpModule,
            ],
            providers: [
                CommonService,
                { provide: XHRBackend, useClass: MockBackend },
            ]
        });
    });

    it('should be created',
        inject([CommonService], (service: CommonService) => {
            expect(service).toBeTruthy();
    }));

    it('should be created',
        inject([CommonService], (service: CommonService) => {
            spyOn(service, 'getTour').and.callThrough();
            service.getTour();
            expect(service.getTour).toHaveBeenCalled();
    }));
});
