import { Injectable } from '@angular/core';
import {
    Http,
    Response,
} from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Tour } from '../../model/tour';
import { Tours } from '../../model/tours';
import { Traveller } from '../../model/traveller';

@Injectable()
export class CommonService {
    private http: Http;
    private selectedTour: Tour;
    private tourId: number;

    constructor(http: Http) {
        this.http = http;
    }

    /**
     * Returns tour data
     */
    public getTour = (): Observable<Tours> => {
        // TODO: change to use api, put in environment files
        return this.http.get('assets/model.json')
            .map((response: Response) => {
              if (!response) {
                throw new Error(
                  'An error has occurred with ' +
                    'retreiving a response for tour data');
              }
            return response.json();
        });
    }

    public setTourId = (route): void => {
        if (!this.tourId) {
            route.parent.params.subscribe(params => {
                this.tourId = params['id'];
            });
        }
    }
}
