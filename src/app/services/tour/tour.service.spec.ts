import { TestBed, inject } from '@angular/core/testing';
import {
  BaseRequestOptions,
  Http,
  HttpModule,
  Response,
  XHRBackend,
} from '@angular/http';

import {MockBackend} from '@angular/http/testing';

import { TourService } from './tour.service';
import { CommonService } from '../services';

describe('TourService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
      ],
      providers: [
        CommonService,
        TourService,
        { provide: XHRBackend, useClass: MockBackend },
      ]
    });
  });

  it('should be created', inject([TourService], (service: TourService) => {
    expect(service).toBeTruthy();
  }));
});
